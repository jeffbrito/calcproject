#ifndef CALENT_H
#define CALENT_H

#include <QMainWindow>

namespace Ui {
class calEnt;
}

class calEnt : public QMainWindow
{
    Q_OBJECT

public:
    explicit calEnt(QWidget *parent = nullptr);
    ~calEnt();

private slots:
    void on_um_clicked();

    void on_dois_clicked();

    void on_tres_clicked();

    void on_quatro_clicked();

    void on_cinco_clicked();

    void on_seis_clicked();

    void on_sete_clicked();

    void on_oito_clicked();

    void on_nove_clicked();

    void on_zero_clicked();

    void on_soma_clicked();

    void on_subtracao_clicked();

    void on_multiplicacao_clicked();

    void on_divisao_clicked();

    float mostrar();
private:
    Ui::calEnt *ui;
};

#endif // CALENT_H
